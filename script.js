/** Aprendiendo Javascript - MeetAndCode 
 * 
 * Ejercicios a resolver
 * 
*/

/** 1 - Muestra en tu navegador una alerta con el mensaje que prefieras. 
 * (Pista: el comando a utilizar es ‘alert’)
 */

 

 /** 2 -Muestra un mensaje por la consola del inspector de tu navegador! 
  * (Pista: el comando a utilizar es ‘console.log’)
  * 
  * Cada navegador web tiene una herramienta para programadores 
  * y la llamamoss "inspector"
 */

 

 /** 3 - Crea una variable llamada “nombre”, asignale tu nombre y muestralo por la consola.
  *  
  *  Las variables son como contenedores que guardan un valor.
  *  Una variable es de un tipo y tiene un nombre y un valor.
  *  Algunos tipos son:
  *  - numericos: 2, 2.5.
  *  - logicos: true, false
  *  - cadenas de caracteres: "Hola Mundo"
  *  
  *  Las variables en Javascript no tiene un tipo definido.
  * 
  *  Para definir variables usamos: var y let.
 */




 /** 4 - Calcula tu edad en años y muestralo en un alert.*/




 /** 5 - Completar el curriculum con tus datos.*/

 /** 6 - Usando javascript, agregar la siguiente información junto al título y subtítulo :
        a) País y provincia
        b) Tu edad
*/

